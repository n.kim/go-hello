FROM golang:1.17 AS build
WORKDIR /
ADD main.go ./
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app ./main.go

FROM alpine:latest
COPY --from=build /app ./
CMD ["./app"]
